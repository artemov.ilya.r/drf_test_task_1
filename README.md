- ### [Задание](docs/task.md)
- ###  [Описание проекта](docs/desc.md)
- ###  [Описание API](docs/api.md)
- ###  [Схема БД](docs/db.md)
### Deploy:

Создать .env
```shell
cp .env.example .env
```
env example:
```
POSTGRES_USER=admin 
POSTGRES_PASSWORD=admin
POSTGRES_DB=testdb
POSTGRES_INITDB_ARGS=-A md5
PGDATA=data

API_KEY=66cd1eb167094d30afc67127070c53c4 | Статический ключ для API drf

PASTEBIN_API_KEY=riusr5... | dev key
PASTEBIN_USER_API_KEY=4d94.... | user_key != dev_key, этот получается в API pastebin
PASTEBIN_MOCK_URL=http://mock:1337/mock | Если стоит, то будет использоваться заглушка, оставить пустым, чтобы обращаться к pastebin

DEBUG=1
SECRET_KEY=dbaa1_i7%*3r9-=z-+_mz4r-!qeed@(-a_r(g@k8jo8y3r27%m
CELERY_BROKER=redis://redis:6379/0
CELERY_BACKEND=redis://redis:6379/0
```

```shell
docker-compose up --build
```