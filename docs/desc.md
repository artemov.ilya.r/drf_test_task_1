Django + DRF

Apps:
- api: реализует API (DRF) ```/api```
- main_site: реализует главную страничку ```/home```

### APP: api
- pastebin.py
  - Создает глобальный объект ```PastebinEngine```
  - Через него забирается ссылка на pastebin для ```/home```
  - Ставятся задачи celery UpdateMenu и DeleteFromPastebin
- pastebin_client.py
  - Реализует общение с pastebin api (или заглушкой)
- permissions.py
  - Реализует ```permission_class``` с API_KEY
- tasks.py:
  - UpdateMenu - получает актуальное "меню", создает paste
  - DeletefromPastebin - удаляет неактуальную пасту (я думал так можно обойти ограничение на максимальное кол-во паст, но нет, там ограничение на запросы)
- tests/test_api.py
  - Тесты 
### APP: main_site

При заходе на ```/home``` создается формочка с номером телефона и списком всех блюд

Блюда разбиваются на категории с помощью кастомных виджетов в ```forms.py```


Скриншоты

![img.png](img.png)
![img_1.png](img_1.png)