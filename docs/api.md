Дефолтное API ```ModelViewSet```

# Создать новое блюдо

**URL** : `/dish/`

**Method** : `POST`

**Auth required** : Yes

*headers:*
```
x-api-key:<api-key>
```

#### POST DATA:

```json
{
    "name": "test_4",
    "price": 100,
    "category": {
        "name": "CUSTOM_CATEGORY"
    },
    "allergens": [
        {
            "name": "CUSTOM_ALLERGEN"
        }
    ]
}
```
*allergens can be empty*
## Success Response

**Code** : `201 CREATED`

---

# Добавить картинку к блюду

**URL** : `/dish/<pk>/image/`

**Method** : `POST`

**Auth required** : Yes

*headers:*
```
x-api-key:<api-key>
```

#### FORM-DATA:

```
upload: <file>
```

## Success Response

**Code** : `201 CREATED`