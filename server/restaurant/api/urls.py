import logging
import random

from rest_framework.routers import DefaultRouter

from api.models import Allergen, DishCategory, Dish
from api.pastebin import pastebin_engine
from api.views import DishViewSet

router = DefaultRouter()
router.register(r'dish', DishViewSet, basename='dish')
urlpatterns = router.urls

pastebin_engine.update_menu()
logging.warning('Update menu from urls')

# TODO: remove
def init_test_data():
    allergens = [
        Allergen(name='Аллерген 1'),
        Allergen(name='Аллерген 2'),
        Allergen(name='Аллерген 3'),
        Allergen(name='Аллерген 4'),
    ]
    categories = [
        DishCategory(name='Завтрак'),
        DishCategory(name='Первое блюдо'),
        DishCategory(name='Напиток'),
    ]
    dishes = [
        Dish(
            name='Нутовый омлет с тофу и овощами',
            nutritional_value=1000,
            img=None,
            category=categories[0],
        ),
        Dish(
            name='Миндальный «манник»',
            nutritional_value=1000,
            img=None,
            category=categories[0],
        ),
        Dish(
            name='Тыква, фаршированная грибами и картофелем',
            nutritional_value=1000,
            img=None,
            category=categories[1],
        ),
        Dish(
            name='Нут с помидорами 🍅',
            nutritional_value=1000,
            img=None,
            category=categories[1],
        ),
        Dish(
            name='Карри с бататом и киноа',
            nutritional_value=1000,
            img=None,
            category=categories[1],
        ),

        Dish(
            name='Куркума-латте (напиток из куркумы)',
            nutritional_value=1000,
            img=None,
            category=categories[2],
        ),

    ]

    Allergen.objects.all().delete()
    DishCategory.objects.all().delete()
    Dish.objects.all().delete()

    for obj in (allergens + categories + dishes):
        obj.save()

    for d in dishes:
        d.allergens.add(random.choice(allergens))

# init_test_data()