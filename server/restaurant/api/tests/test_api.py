import json
import os
import random
import uuid

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from api.models import Dish, DishCategory, Allergen
from api.serializer import DishSerializer
from restaurant import settings


class DishApiTestCase(APITestCase):
    valid_api_key = settings.API_KEY
    invalid_api_key = uuid.uuid4().hex
    valid_headers = {
        'HTTP_X_API_KEY': valid_api_key,
    }
    invalid_headers = {
        'HTTP_X_API_KEY': uuid.uuid4().hex,
    }

    def setUp(self) -> None:
        self.allergens = [
            Allergen(name='Аллерген 1'),
            Allergen(name='Аллерген 2'),
            Allergen(name='Аллерген 3'),
            Allergen(name='Аллерген 4'),
        ]
        self.categories = [
            DishCategory(name='Завтрак'),
            DishCategory(name='Первое блюдо'),
            DishCategory(name='Напиток'),
        ]
        self.dishes = [
            Dish(
                name='Нутовый омлет с тофу и овощами',
                nutritional_value=1000,
                img=None,
                category=self.categories[0],
                price=random.randint(100, 1000)
            ),
            Dish(
                name='Миндальный «манник»',
                nutritional_value=1000,
                img=None,
                category=self.categories[0],
                price=random.randint(100, 1000)
            ),
            Dish(
                name='Тыква, фаршированная грибами и картофелем',
                nutritional_value=1000,
                img=None,
                category=self.categories[1],
                price=random.randint(100, 1000)
            ),
            Dish(
                name='Нут с помидорами 🍅',
                nutritional_value=1000,
                img=None,
                category=self.categories[1],
                price=random.randint(100, 1000)
            ),
            Dish(
                name='Карри с бататом и киноа',
                nutritional_value=1000,
                img=None,
                category=self.categories[1],
                price=random.randint(100, 1000)
            ),

            Dish(
                name='Куркума-латте (напиток из куркумы)',
                nutritional_value=1000,
                img=None,
                category=self.categories[2],
                price=random.randint(100, 1000)
            ),

        ]

        Allergen.objects.all().delete()
        DishCategory.objects.all().delete()
        Dish.objects.all().delete()

        for obj in (self.allergens + self.categories + self.dishes):
            obj.save()

        for d in self.dishes:
            d.allergens.add(random.choice(self.allergens))

    def test_get_list(self):
        excepted_data = DishSerializer(self.dishes, many=True).data
        url = reverse('dish-list')
        response = self.client.get(url, **self.valid_headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, excepted_data)

        response = self.client.get(url, **self.invalid_headers)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_detail(self):
        for dish in self.dishes:
            url = reverse('dish-detail', args=[dish.pk])
            excepted_data = DishSerializer(dish).data

            response = self.client.get(url, **self.valid_headers)

            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.data, excepted_data)

            response = self.client.get(url, **self.invalid_headers)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_create(self):
        post_data_list = [
            {
                'name': 'test_2',
                'price': 100,
                'category': {
                    'name': 'CUSTOM_CATEGORY'
                }
            },
            {
                'name': 'test_3',
                'price': 100,
                'category': {
                    'name': 'CUSTOM_CATEGORY'
                },
                'allergens': [{
                    'name': 'CUSTOM_ALLERGEN'
                }]
            },
            {
                'name': 'test_4',
                'price': 100,
                'category': {
                    'name': 'CUSTOM_CATEGORY'
                },
                'allergens': [{
                    'name': 'CUSTOM_ALLERGEN'
                }]
            },
        ]
        url = reverse('dish-list')

        for post_data in post_data_list:
            response = self.client.post(url, post_data, format='json', **self.valid_headers)
            body = json.loads(response.content)
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual('id' in body, True)

            from django.core.files.uploadedfile import SimpleUploadedFile
            print(os.getcwd())
            file = open('api/tests/test_data/mock.jpg', 'rb')
            upload = SimpleUploadedFile('mock.jpg', file.read(), content_type='image/jpg')

            # TODO: how to user reverse?
            url_image = f'/api/dish/{body["id"]}/image/'

            response = self.client.post(url_image, {'upload': upload}, **self.valid_headers, format='multipart')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            file.close()

        response = self.client.post(url, random.choice(post_data_list), **self.invalid_headers)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
