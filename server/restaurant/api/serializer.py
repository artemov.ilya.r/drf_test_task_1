from rest_framework.serializers import ModelSerializer

from api.models import Dish, Allergen, DishCategory


class AllergenSerializer(ModelSerializer):
    class Meta:
        model = Allergen
        fields = '__all__'

    def to_internal_value(self, data):
        if data and data.get('name'):
            obj, _ = self.Meta.model.objects.get_or_create(name=data['name'])
            return obj
        return super().to_internal_value(data)


class DishCategorySerializer(ModelSerializer):
    class Meta:
        model = DishCategory
        fields = '__all__'

    def to_internal_value(self, data):
        if data and data.get('name'):
            obj, _ = self.Meta.model.objects.get_or_create(name=data['name'])
            return obj
        return super().to_internal_value(data)


class DishSerializer(ModelSerializer):
    category = DishCategorySerializer()
    allergens = AllergenSerializer(many=True, required=False)

    class Meta:
        model = Dish
        fields = ['id', 'name', 'category', 'allergens', 'price', 'img']

    def create(self, validated_data):
        allergens = validated_data.pop('allergens') if 'allergens' in validated_data else None
        category = validated_data.pop('category')

        dish = Dish.objects.create(
            category=category,
            **validated_data
        )
        if allergens:
            dish.allergens.set(allergens)

        dish.save()
        # # TODO: on update model NOT IMPLEMENTED
        # # TODO: update from modifying/creation from admin panel NOT IMPLEMENTED
        # pastebin_engine.update_menu()
        return dish
