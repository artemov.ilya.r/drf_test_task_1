import logging

import requests
import re
from restaurant import settings


class PastebinApiClient:
    # url = settings.PASTEBIN_MOCK_URL or 'https://pastebin.com/api/api_post.php'
    url = 'http://0.0.0.0:1337/mock'
    list_result_regex_pattern = re.compile(r'<paste_key>(.+)<\/paste_key>')

    def __make_request(self, **kwargs):
        response = None
        try:
            data = {
                'api_dev_key': settings.PASTEBIN_API_KEY,
                'api_user_key': settings.PASTEBIN_USER_API_KEY
            }
            logging.info(f'Request to {self.url} with body={kwargs}')
            response = requests.post(self.url, data={**data, **kwargs})
        except Exception as e:
            logging.error(e)
        if response and response.status_code == 200:
            logging.info(f'Success {kwargs.get("api_option", "unknown method")}')
            if isinstance(response.content, bytes):
                return response.content.decode()
            else:
                return response.content
        else:
            if response:
                logging.error(f'Failed {kwargs.get("api_option", "unknown method")},'
                              f'{response.status_code=}, {response.content=}')
            else:
                logging.error(
                    f'Failed {kwargs.get("api_option", "unknown method")}')

    def delete_paste(self, paste_key):
        return self.__make_request(
            api_paste_key=paste_key,
            api_option='delete'
        )

    def create_paste(self, text):
        return self.__make_request(
            api_paste_code=text,
            api_option='paste'
        )

    def paste_list(self):
        try:
            str_xml_result = self.__make_request(
                api_option='list',
                api_results_limit=100
            )
            return re.findall(self.list_result_regex_pattern, str_xml_result)
        except:
            ...
        return []

    def delete_all(self):
        for paste_key in self.paste_list():
            self.delete_paste(paste_key)
