import logging

import celery

from api.models import DishCategory, Dish
from api.pastebin_client import PastebinApiClient

from restaurant.celery import app


class UpdateMenu(celery.Task):
    def run(self, *args, **kwargs):
        return self.__update_menu()

    def __init__(self):
        self.pastebin_client = PastebinApiClient()
        self.paste_url = None
        super(UpdateMenu, self).__init__()

    def __update_menu(self):
        self.menu = {
            # expensive
            category: Dish.objects.filter(category=category)
            for category in DishCategory.objects.all()
        }
        return self.pastebin_client.create_paste(self.__menu_to_text())

    def __menu_to_text(self):
        text = ''
        for category, dishes in self.menu.items():
            text += category.name + '\n'
            for dish in dishes:
                text += f'\t {dish.name} [{dish.price}₽]\n'
        return text


class DeleteFromPastebin(celery.Task):
    def __init__(self):
        self.pastebin_client = PastebinApiClient()

    def run(self, *args, **kwargs):
        return self.__delete(*args)

    def __delete(self, url):
        logging.warning(f'Start deleting paste: {url}')
        return self.pastebin_client.delete_paste(url.split('/')[-1]) if url else None


UpdateMenu = app.register_task(UpdateMenu())
DeleteFromPastebin = app.register_task(DeleteFromPastebin())
