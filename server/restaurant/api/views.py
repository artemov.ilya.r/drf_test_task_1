from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework.decorators import parser_classes
from rest_framework.parsers import FormParser, MultiPartParser

from api.models import Dish
from api.permissions import StaticAPIKeyPermission
from api.serializer import DishSerializer


class DishViewSet(ModelViewSet):
    queryset = Dish.objects.all()
    serializer_class = DishSerializer
    permission_classes = [StaticAPIKeyPermission]

    @action(methods=['post'], permission_classes=[StaticAPIKeyPermission], detail=True)
    @parser_classes((FormParser, MultiPartParser,))
    def image(self, request, *args, **kwargs):
        if 'upload' in request.data:
            dish = self.get_object()
            dish.img.delete()

            upload = request.data['upload']

            dish.img.save(upload.name, upload)

            return Response(status=HTTP_201_CREATED, headers={'Location': dish.img.url})
        else:
            return Response(status=HTTP_400_BAD_REQUEST)
