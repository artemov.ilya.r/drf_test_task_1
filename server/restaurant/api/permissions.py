from rest_framework import permissions

from restaurant import settings


class StaticAPIKeyPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.headers.get('x-api-key') == settings.API_KEY
