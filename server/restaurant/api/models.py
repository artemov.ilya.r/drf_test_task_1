from django.db import models
from django.db.models.signals import post_save


class Dish(models.Model):
    name = models.CharField(max_length=256)
    nutritional_value = models.IntegerField(blank=True, null=True)
    price = models.IntegerField()

    img = models.ImageField(upload_to='images/', blank=True, null=True)
    category = models.ForeignKey('DishCategory', on_delete=models.PROTECT)
    allergens = models.ManyToManyField('Allergen', blank=True, null=True)

    def __str__(self):
        return f'[{self.category}] {self.name}'


def update_menu(sender, instance, **kwargs):
    from api.pastebin import pastebin_engine
    pastebin_engine.update_menu()


post_save.connect(update_menu, sender=Dish)


class Allergen(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name


class DishCategory(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name


class Order(models.Model):
    phone = models.CharField(max_length=16)
    dishes = models.ManyToManyField(Dish)

    def __str__(self):
        dishes_str = ', '.join(d.name for d in self.dishes.all())
        if len(dishes_str) > 100:
            dishes_str = dishes_str[:100] + '...'
        repr_str = f'[{self.phone}]: {dishes_str}'
        return repr_str
