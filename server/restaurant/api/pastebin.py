import logging

from collections import deque

from api.tasks import DeleteFromPastebin, UpdateMenu


class PastebinEngine:

    def __init__(self):
        self.tasks = deque(maxlen=10)
        self.cached_result = None

    def update_menu(self):
        logging.info('new celery task: UpdateMenu')
        self.tasks.append(UpdateMenu.delay())

    def get_paste_url(self):
        logging.warning(f'Try to get paste url from celery')
        logging.warning(self.__log_task_queue())
        for task in reversed(self.tasks):
            if task.status == 'SUCCESS':
                self.cached_result = task.result
                logging.warning(f'Success getting pastebin url: {self.cached_result=}')
                self.__log_task_queue()
                self.__clean_old()
                return self.cached_result
        return self.cached_result

    def __log_task_queue(self):
        logging.warning('Task queue: [')
        for task in self.tasks:
            logging.warning(f'\t {self.__task_to_log(task)}')
        logging.warning(']')

    def __task_to_log(self, task):
        return {
            'id': task.task_id,
            'status': task.status,
            'state': task.state,
            'result': task.result,
        }

    def __clean_old(self):
        tasks_to_remove = []
        for task in self.tasks:
            if (
                    task.status == 'SUCCESS' and
                    task.result and
                    task.result != self.cached_result
            ):
                tasks_to_remove.append(task)
        logging.warning(f'tasks to remove: {tasks_to_remove}')
        for task in tasks_to_remove:
            self.__log_task_queue()
            logging.warning(f'new celery task: DeleteFromPastebin: {task.result}')
            DeleteFromPastebin.delay(task.result)
            self.tasks.remove(task)
        logging.warning('Final')
        self.__log_task_queue()


pastebin_engine = PastebinEngine()
