from django.contrib import admin

from api.models import Dish, Allergen, DishCategory, Order


@admin.register(Dish)
class DishAdmin(admin.ModelAdmin):
    ...


@admin.register(Allergen)
class AllergenAdmin(admin.ModelAdmin):
    ...


@admin.register(DishCategory)
class DishCategoryAdmin(admin.ModelAdmin):
    ...


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    ...
