from django.shortcuts import render

from api.pastebin import pastebin_engine
from main_site.forms import OrderForm


# cache if need with cache_page
def DishListPage(request):
    send_success = False
    total_sum = None
    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            form.save()
            send_success = True
            total_sum = form.total_sum

    form = OrderForm()
    context = {
        'total_sum': total_sum,
        'form': form,
        'success': send_success,
        'pastebin_menu_link': pastebin_engine.get_paste_url(),
    }
    return render(request, 'home/home.html', context)
